#
# BSD 3-Clause License
#
# Copyright (c) 2018, Tomek D. Loboda All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
#    disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
#    following disclaimer in the documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
#    products derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
# INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
# USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# ----------------------------------------------------------------------------------------------------------------------

import os
import sys
from inspect import getsourcefile

__file__ = os.path.abspath(getsourcefile(lambda: None))
sys.path.append(os.path.dirname(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


import queue


from ga3c_tetris.Config      import Config
from ga3c_tetris.Environment import Environment
from led_tetris              import Tetris


# ======================================================================================================================
class TetrisEnvGA3C(Environment):
    '''
    An extension of the Environment from the NVIDIA's GA3C RL algorithm implementation.
    '''

    game = None

    # ------------------------------------------------------------------------------------------------------------------
    def __init__(self, seed=None):
        ''' Overriding because we need Tetris object instead of GameManager one. '''

        self.game = Tetris(is_human=False, do_vis=False, seed=seed)
        self.game.app_start()

        self.nb_frames      = Config.STACKED_FRAMES
        self.frame_q        = queue.Queue(maxsize=self.nb_frames)
        self.previous_state = None
        self.current_state  = None
        self.total_reward   = 0

        self.reset()

    # ------------------------------------------------------------------------------------------------------------------
    def __del__(self):
        self.game.app_end()

    # ------------------------------------------------------------------------------------------------------------------
    def _update_frame_q(self, frame):
        ''' Overriding because we do not need to preprocess images. '''

        if frame is None: return

        if self.frame_q.full():
            self.frame_q.get()
        self.frame_q.put(frame)

    # ------------------------------------------------------------------------------------------------------------------
    @staticmethod
    def get_num_actions():
        ''' Overriding to avoid instantiating the game. '''

        return len(Tetris.ACTIONS)

    # ------------------------------------------------------------------------------------------------------------------
    def reset(self, do_vis=False):
        ''' Overriding because the game's reset() method is called differently.  '''

        self.total_reward = 0
        self.frame_q.queue.clear()
        self._update_frame_q(self.game.game_start(do_vis))
        self.previous_state = None
        self.current_state  = None

    # ------------------------------------------------------------------------------------------------------------------
    def step(self, action):
        ''' Overriding because the game's step() method is called differently.  '''
        state, reward, done, _ = self.game.game_do_action(action)

        self.total_reward += reward
        self._update_frame_q(state)

        self.previous_state = self.current_state
        self.current_state  = self._get_current_state()

        return reward, done
