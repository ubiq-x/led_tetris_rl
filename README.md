# LED Tetris RL
A3C reinforcement learning for LED Tetris

## Introduction
This is a slight modification of the NVIDIA's [GA3C](https://github.com/NVlabs/GA3C) implementation of the A3C (Asynchronous Advantage Actor-Critic) reinforcement learning algorithm.  The changes are quite minimal and are commented in the code; their sole purpose is to accommodate the [LED Tetris](https://gitlab.com/ubiq-x/led_tetris) game as the environment.


## Dependencies
- [Python 3](https://www.python.org)
- [GA3C](https://github.com/NVlabs/GA3C) (with slight modifications)
- [LED Tetris](https://gitlab.com/ubiq-x/led_tetris)
- [LED Matrix](https://gitlab.com/ubiq-x/led_matrix)

## Setup
This section addresses the learning machine setup.  If you want to see the trained agent play, refer here: [LED Tetris](https://gitlab.com/ubiq-x/led_tetris).

### Software
If you're on MacOS, make sure to have [HomeBrew](https://brew.sh) before continuing.

First, initialize a new Python venv and install the requirements (note that these are all of the project's requirements, including the ones for the GA3C):
```sh
venv=led_tetris_rl
python=python3.6

mkdir -p ~/prj/
cd ~/prj
$python -m venv $venv
cd $venv
source ./bin/activate

case "$(uname)" in
    "Darwin")
        brew update
        brew install cmake
        ;;
    "FreeBSD")  # 12R + py36
        pkg update
        pkg upgrade
        pkg install -y blas lapack gcc py36-numpy py36-scipy py36-pillow
        
        # py35:
        # pkg install -y blas lapack gcc py35-numpy
        # cd /usr/ports/graphics/py-pillow && make install clean
        ;;
    "Linux")
        sudo apt-get update
        sudo apt-get upgrade
        sudo apt-get install cmake
        ;;
    "Windows") echo "Get a real OS";;
esac

easy_install -U pip
pip3 install --upgrade numpy scipy tensorflow Pillow atari-py gym

mkdir -p src
cd src
git clone https://gitlab.com/ubiq-x/led_matrix
git clone https://gitlab.com/ubiq-x/led_tetris
git clone https://gitlab.com/ubiq-x/led_tetris_rl
mv led_tetris_rl/ga3c_tetris ./
```

Then, initiate the A3C algorithm like so:
```sh
cd ga3c_tetris
sh _clean.sh
sh _train.sh
```


## Trained Agent
If you'd prefer to skip right to seeing the agent play, simply rename the `checkpoints-trained` directory to `checkpoints` and run `sh _play.sh`.  Naturally, this will only work on a computer with the LED matrix connected.  Don't expect impeccable performance though; you have been warned.


## Gameplay
[This short video](https://www.youtube.com/watch?v=9fVr0SSIT1Y) shows a moment of the gameplay.  The agent makes mistakes leading to it loosing the game.  It could be improved, but it's good enough as a proof of concept.


## License
This project is licensed under the [BSD License](LICENSE.md).
