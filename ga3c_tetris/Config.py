# Copyright (c) 2016, NVIDIA CORPORATION. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#  * Neither the name of NVIDIA CORPORATION nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
# OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

class Config:
    # (1) Game configuration:
    ATARI_GAME = 'PongDeterministic-v0'  # e.g. PongDeterministic-v0

    PLAY_MODE = False
    TRAIN_MODELS = True
    LOAD_CHECKPOINT = False
    LOAD_EPISODE = 0  # load last checkpoint

    AGENTS = 8  # 32  # if the dynamic configuration is on, these are the initial values
    PREDICTORS = 2
    TRAINERS = 2

    DEVICE = 'gpu:0'

    DYNAMIC_SETTINGS = True  # enable the dynamic adjustment (+ waiting time to start it)
    DYNAMIC_SETTINGS_STEP_WAIT = 20
    DYNAMIC_SETTINGS_INITIAL_WAIT = 10

    # (2) Algorithm parameters:
    DISCOUNT = 0.99

    TIME_MAX = 5  # n-step

    REWARD_MIN = -1
    REWARD_MAX = 1

    MAX_QUEUE_SIZE = 100
    PREDICTION_BATCH_SIZE = 128

    STACKED_FRAMES = 4
    IMAGE_WIDTH = 8
    IMAGE_HEIGHT = 8

    EPISODES = 5000000
    ANNEALING_EPISODE_COUNT = 10000

    BETA_START = 0.01  # entropy regualrization hyper-parameter
    BETA_END = 0.01

    LEARNING_RATE_START = 0.0003
    LEARNING_RATE_END = 0.0003

    RMSPROP_DECAY = 0.99
    RMSPROP_MOMENTUM = 0.0
    RMSPROP_EPSILON = 0.1

    DUAL_RMSPROP = False  # we found that using a single RMSProp for the two cost function works better and faster

    USE_GRAD_CLIP = False
    GRAD_CLIP_NORM = 40.0
    LOG_EPSILON = 1e-6  # epsilon (regularize policy lag in GA3C)
    TRAINING_MIN_BATCH_SIZE = 0
        # training min batch size - increasing the batch size increases the stability of the algorithm, but makes
        # learning slower

    # (3) Log and save:
    TENSORBOARD = True
    TENSORBOARD_UPDATE_FREQUENCY = 1000

    SAVE_MODELS = True
    SAVE_FREQUENCY = 1000

    PRINT_STATS_FREQUENCY = 10  # 1
    STAT_ROLLING_MEAN_WINDOW = 10

    RESULTS_FILENAME = 'res-tetris.txt'  # 'results.txt'
    NETWORK_NAME = 'net-tetris'  # 'network'

    # (4) Experimental parameters:
    MIN_POLICY = 0.0  # minimum policy
    USE_LOG_SOFTMAX = False  # use log_softmax() instead of log(softmax())
